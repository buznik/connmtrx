/**
 *	Store connection data
 */ 
module.exports = function connectionMatrix() {
	this.data = {
		fd: {},
		id: {},
		client_id: {},
		serial: {},
	};

	this.add = function(fd, owner, client_id) {
		if (!fd || !owner || !client_id) return false;

		var ezlos = false;
		
		this.data.fd[fd] = {
			id: owner._id,
			client_id: client_id,
			ezlos: owner.ezlos
		}
		this.data.id[owner._id] = {
			fd: fd,
			client_id: client_id,
			ezlos: owner.ezlos
		}
		this.data.client_id[client_id] = {
			id: owner._id,
			fd: fd,
			ezlos: owner.ezlos
		}
		if (typeof owner.ezlos != "undefined") {
			owner.ezlos.forEach(function(ezlo) {
				this.data.serial[ezlo.serial] = {
					fd: fd,
					client_id: client_id,
					id: owner._id
				}
			}, this);
		}
	}

	this.get = function(property, value) {
		if (!property) return false;
		if (!value) return this.data;

		if (typeof this.data[property] != "undefined") {
			if (typeof this.data[property][value] != "undefined") {
				return this.data[property][value];
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	this.remove = function(fd) {
		if (!fd) return false;

		var owner = this.get("fd", fd);

		if (typeof owner.ezlos != "undefined") {
			owner.ezlos.forEach(function(ezlo) {
				delete this.data.serial[ezlo.serial];
			}, this);
		}
		delete this.data.id[owner.id];
		delete this.data.client_id[owner.client_id];
		delete this.data.fd[fd];
	}

};